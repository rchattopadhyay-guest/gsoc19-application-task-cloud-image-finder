$(document).ready(function(){
    //change on click on filter
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
        $filters = $panel.find('.filters input'),
        $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });
    $('.mainSearch').keyup(function(e){
        // Ignore tab key
        var code = e.keyCode || e.which;
        if (code == '9') return;
        // Useful DOM data and selectors
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        // No result check
        $table.find('tbody .no-result').remove();
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="8">No result found</td></tr>'));
            // $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
    //just for development search
    $('.filterable .filters input').keyup(function(e){
        // Ignore tab key
        var code = e.keyCode || e.which;
        if (code == '9') return;
        // Useful DOM data and selectors
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        // No result check
        $table.find('tbody .no-result').remove();
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="8">No result found</td></tr>'));
            // $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
    });
});
//sidebar 
function openNav() {
    document.getElementById("sideBar").style.opacity = 1;
    if(screen.width<400) {
        document.getElementById("sideBar").style.width = "100%";
        return;
    }
    else  document.getElementById("sideBar").style.width = "350px";
    document.getElementById("main").style.marginRight = "350px"; 
    document.getElementsByTagName("BODY")[0].style.overflow = "hidden";
  }
  
  function closeNav() {
    document.getElementById("sideBar").style.width = "0";
    document.getElementById("sideBar").style.opacity = 0;
    document.getElementById("main").style.marginRight = "0";
    document.getElementsByTagName("BODY")[0].style.overflow = "visible";
  }
  //close sidebar if escape is pressed
  document.onkeydown = function(evt) {
      //if already closed
      if(document.getElementById("sideBar").style.opacity == 0) return;
    evt = evt || window.event;
    var isEscape = false;
    if ("key" in evt) {
        isEscape = (evt.key === "Escape" || evt.key === "Esc");
    } else {
        isEscape = (evt.keyCode === 27);
    }
    if (isEscape) {
        closeNav();
    }
};